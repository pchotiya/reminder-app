import uuid from "react-uuid";
import { Types } from "../Constants/ActionTypes";
import { getTodoItemType } from "../utils/helper";

const initialState = {"alltodo": [],"priorities": {
    "High": "#FECACA",
    "Medium": "#FDE68A",
    "Low": "#E5E7EB"
}};

export const TodoReducer = (state=initialState,action) => {
    switch(action.type){
        case Types.ADDTODO:
            const TodoItemType = "alltodo";
            
            // if(getTodoItemType(action.payload.todoObject.timestamp)){
            //     TodoItemType = "upcoming";
            // }
            // else{
            //     TodoItemType = "past"
            // }
            
            const newTodoItem = {
                id: uuid(),
                task: action.payload.todoObject.task,
                timestamp: action.payload.todoObject.timestamp,
                status: action.payload.todoObject.status,
                priority: action.payload.todoObject.priority
            }

            console.log("todoitem: ",newTodoItem);
            return {
                ...state,
                [TodoItemType]: [...state[TodoItemType],newTodoItem]
            };
        case Types.EDITTODO:
            let todoItemType = action.payload.todoItemType;
            let id = action.payload.id;
            let editTodoObject = action.payload.payloadObject;
            let TodoArray = state[todoItemType];
            // let evaluateTodoItemType = todoItemType;

            // if(getTodoItemType(editTodoObject.timestamp)){
            //     evaluateTodoItemType = "upcoming";
            // }
            // else{
            //     evaluateTodoItemType = "past";
            // }

            // if(todoItemType !== evaluateTodoItemType){
            //     let newTodoArray = state[evaluateTodoItemType];
            //     TodoArray = TodoArray.filter(todoItem => {
            //         if(todoItem.id === id){
            //             newTodoArray.push(todoItem);
            //         }
            //         return todoItem.id !== id
            //     });

                // console.log("arrays: ",TodoArray,newTodoArray);
                // return {
                //     ...state,
                //     [todoItemType]: TodoArray,
                // }
            // }

            // console.log("todoItem: ",todoItemType);
            
            
            TodoArray.map((todoItem) => {
                if(id === todoItem.id){
                    todoItem.task = editTodoObject.task;
                    todoItem.timestamp = editTodoObject.timestamp;
                    todoItem.status = editTodoObject.status;
                }
            });

            console.log("status reducer: ",TodoArray);

            return {    
                ...state,
                [todoItemType]: TodoArray
            };
        case Types.DELETETODO:
            const idToBeDeleted = action.payload.id;
            const todoType = action.payload.todoItemType;
            let todoArray = state[todoType];
            todoArray = todoArray.filter(todoItem => todoItem.id !== idToBeDeleted)

            return {
                ...state,
                [todoType]: todoArray
            }
        default: return state;
    }
}